package com.mypackage.bigdata;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.yarn.conf.YarnConfiguration;

public class MatrixMultiply {
	public static class MartrixMap extends Mapper<Object, Text, Text, Text> {
		private Text mapKey = new Text();
		private Text mapValue = new Text();
		int rowNum = 4;// 行
		int cloNum = 2;// 列 
		// input格式：A#1#2#3->表示第1行第2列的数为3
		// output格式：<行#列，(矩阵 )# (该行或该列第n个元素） #（第n个元素值）
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			// A[i,j],i,j表示二维A矩阵下标 ;B[j,k],j,k表示二维B矩阵下标
			String i, j, k, matrixA_ij, matrixB_jk;
			String strline[] = value.toString().split("#");
			// 标记，tag值可为：A,B（对应A,B矩阵）
			String tag = strline[0];
			// 分别把矩阵A，B按顺序存到第矩阵C中要操作的第几行或第几列的第n个数
			if (tag.equals("A")) {
				i = strline[1];
				j = strline[2];
				matrixA_ij = strline[3];
				// 把矩阵A输出到C矩阵操作位置：eg：1#1 A#1#2
				for (int p = 1; p <= cloNum; p++) {// 列
					mapKey.set(i + "#" + String.valueOf(p));
					mapValue.set("A" + "#" + j + "#" + matrixA_ij);
					context.write(mapKey, mapValue);
				}
			} else {
				j = strline[1];
				k = strline[2];
				matrixB_jk = strline[3];
				// 把矩阵B输出到C矩阵操作位置：eg：1#1 B#1#2
				for (int q = 1; q <= rowNum; q++) {// 行
					mapKey.set(String.valueOf(q) + "#" + k);
					mapValue.set("B" + "#" + j + "#" + matrixB_jk);
					context.write(mapKey, mapValue);
				}
			}
		}
	}

	public static class MartrixReduce extends Reducer<Text, Text, Text, Text> {
		private Text reduceValue = new Text();
		// jNum 为矩阵A：n*m 矩阵B:m*k 中的m
		int jNum = 3;
		int matrixA_ij[] = new int[jNum + 1];
		int matrixB_jk[] = new int[jNum + 1]; 
		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			int j, sum_ij = 0;
			/*
			 * 对map的输出结果（格式eg：1#1 B#2#0），取value值eg：B#2#0进行处理
			 * 格式解释就是：对结果矩阵C的第1行第1列值，
			 * 有矩阵A的第1行的所有值分别对应和矩阵B第1列的所有值相乘再相加（共jNum对相乘然后结果相加）
			 */
			for (Text val : values) {
				String strLine[] = val.toString().split("#");
				// tag值可为A||B
				String tag = strLine[0];
				j = Integer.parseInt(strLine[1]);
				/*
				 * for循环对于Text val会遍历相同key进行操作，即对A,B相同key （eg：1#1）进行value操作，
				 * 所以取出values值都是针对A,B中不同行列（由key决定了）的相同位置的值。
				 * 分别取矩阵A（eg：第1行第j个数）和矩阵B（eg：第1列第j个数）也就是矩阵C中同行同列，
				 * 所有要操作的值存到数组matrixA_ij,matrixB_jk中
				 */
				if (tag.equals("A")) {
					matrixA_ij[j] = Integer.parseInt(strLine[2]);
				} else {
					matrixB_jk[j] = Integer.parseInt(strLine[2]);
				}
			}
			/*
			 * 对矩阵A和B相同key值（第X行第y列）的元素相乘后进行累加eg: 1#1 A#2#3,1#1 B#2#1,比如这样的输出，key
			 * 1#1就是矩阵C第1行第1列，A的第2个数与B的第2个数相乘1*3,同理对A,B第1个到第jNum个分别相乘再累加，
			 * 作为矩阵C 第1行第1列（1#1）的value值，然后输出
			 */
			for (int i = 1; i <= jNum; i++) {
				sum_ij += matrixA_ij[i] * matrixB_jk[i];
			}
			reduceValue.set(String.valueOf(sum_ij));
			context.write(new Text(key + ":"), reduceValue);
		}

	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new YarnConfiguration();
		conf.set("fs.defaultFS", "hdfs://192.168.70.14:9000");
		conf.set("yarn.resourcemanager.address", "192.168.70.14:8032");
		conf.set("yarn.resourcemanager.scheduler.address", "192.168.70.14:8030");
		conf.set("mapreduce.framework.name", "yarn");
		conf.set("mapreduce.job.jar",
				"D://软件安装//Users//win7//MatrixMultiply.jar");
		conf.set("mapreduce.app-submission.cross-platform", "true");
		String[] ioArgs = new String[] {
				"hdfs://192.168.70.14:9000/user/hadoop/matrix-multiply/matrix.txt",
				"hdfs://192.168.70.14:9000/user/hadoop/matrix-multiply/output" };

		Job job = new Job(conf, "MatrixMultiply");
		job.setJarByClass(MatrixMultiply.class);
		job.setMapperClass(MartrixMap.class);
		// job.setReducerClass(MartrixReduce.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		FileInputFormat.addInputPath(job, new Path(ioArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(ioArgs[1]));

		System.exit(job.waitForCompletion(true) ? 0 : 1);

	}
}