package mypackage;
//package org.apache.hadoop.examples;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.yarn.conf.YarnConfiguration;

public class BayesWordCount {

	public static class TokenizerMapper extends
			Mapper<Object, Text, Text, IntWritable> {

		private final static IntWritable one = new IntWritable(1);
		private Text word = new Text();

		public void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {
			String lines=value.toString();
			String liness=lines.toLowerCase();
			String linesss=liness.replaceAll("\\t|\\(|\\)|\\|\\.|\\,|\\:|\\[|\\]|\\?|\\--|\\;|\\!|\\'|\\*|/|&|-|\\|", " ");
			StringTokenizer itr = new StringTokenizer(linesss);
			while (itr.hasMoreTokens()) {
				word.set(itr.nextToken());
				context.write(word, one);
			}
		}
	}

	public static class IntSumReducer extends
			Reducer<Text, IntWritable, Text, Text> {
		private IntWritable result = new IntWritable();

		public void reduce(Text key, Iterable<IntWritable> values,
				Context context) throws IOException, InterruptedException {
			//Text keyNew = new Text(key+ ":");
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			result.set(sum);
			context.write(new Text(key+":"+result),new Text(" "));
		}
	}
	
	public static class CombinerClass extends Reducer<Text, IntWritable, Text, IntWritable>{ 
		private IntWritable result = new IntWritable();
		public void reduce(Text key, Iterable<IntWritable> values,
				Context context) throws IOException, InterruptedException {
			
			//Text keyNew = new Text(key+ ":");
			int sum = 0;
			for (IntWritable val : values) {
				sum += val.get();
			}
			result.set(sum);
			context.write(key,result);
		}
	}

	
	public static void main(String[] args) throws Exception { 
		Configuration conf = new YarnConfiguration(); 
		conf.set("fs.defaultFS", "hdfs://192.168.70.14:9000");
		conf.set("yarn.resourcemanager.address", "192.168.70.14:8032");
		conf.set("yarn.resourcemanager.scheduler.address", "192.168.70.14:8030");
		conf.set("mapreduce.framework.name", "yarn");
		conf.set("mapred.jar", "D://软件安装//Users//win7//BayesDocument.jar"); 
		conf.set("mapreduce.app-submission.cross-platform", "true");
		String[] ioArgs = new String[]{
//				"hdfs://192.168.70.14:9000/user/hadoop/bayes-input/train/tagC", 
//				"hdfs://192.168.70.14:9000/user/hadoop/bayes-input/train/output1"
//				"hdfs://192.168.70.14:9000/user/hadoop/bayes-input/train/tagJ", 
//				"hdfs://192.168.70.14:9000/user/hadoop/bayes-input/train/output2"
//				"hdfs://192.168.70.14:9000/user/hadoop/bayes-input/test/1", 
//				"hdfs://192.168.70.14:9000/user/hadoop/bayes-input/test/output1"
//				"hdfs://192.168.70.14:9000/user/hadoop/bayes-input/test/2", 
//				"hdfs://192.168.70.14:9000/user/hadoop/bayes-input/test/output2"
				"hdfs://192.168.70.14:9000/user/hadoop/bayes-input/test/3", 
				"hdfs://192.168.70.14:9000/user/hadoop/bayes-input/test/output3"
//				"hdfs://192.168.70.14:9000/user/hadoop/bayes-input/test/4", 
//				"hdfs://192.168.70.14:9000/user/hadoop/bayes-input/test/output4"
		};
		Job job = Job.getInstance(conf, "word count");
		job.setJarByClass(BayesWordCount.class); 
		job.setMapperClass(TokenizerMapper.class);
		job.setCombinerClass(CombinerClass.class);
		job.setReducerClass(IntSumReducer.class); 
		job.setOutputKeyClass(Text.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);
		job.setOutputValueClass(Text.class);
		job.setOutputKeyClass(IntWritable.class);  
		FileInputFormat.addInputPath(job, new Path(ioArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(ioArgs[1]));
		System.exit(job.waitForCompletion(true) ? 0 : 1);
		//job.waitForCompletion(true);如果多个程序连在一起运行
	}
}
