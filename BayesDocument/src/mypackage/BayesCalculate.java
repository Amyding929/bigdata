package mypackage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.yarn.conf.YarnConfiguration;

public class BayesCalculate {
	public static Map<String, Double> tagC = new HashMap<String, Double>();
	public static Map<String, Double> tagJ = new HashMap<String, Double>();
	public static Map<String, Double> test1 = new HashMap<String, Double>();
	public static Map<String, Double> test2 = new HashMap<String, Double>();
	public static Map<String, Double> test4 = new HashMap<String, Double>();
	public static Integer sizes;
	public static Map<String, Double> tagCJ = new HashMap<String, Double>();

	public static void getData() {
		Configuration conf = configFun();
		FileSystem fs;
		try {
			fs = FileSystem.get(conf);
			// get test score compare real score
			Path outputC = new Path(
					"/user/hadoop/bayes-input/train/output1/part-r-00000");//  TAGC
			Path outputJ = new Path(
					"/user/hadoop/bayes-input/train/output2/part-r-00000");// 
			Path outputTest1 = new Path(
					"/user/hadoop/bayes-input/test/output1/part-r-00000");//  TAGC
			Path outputTest2 = new Path(
					"/user/hadoop/bayes-input/test/output2/part-r-00000");//  
			Path outputTest4 = new Path(
					"/user/hadoop/bayes-input/test/output3/part-r-00000");//  
			tagC = readFile(outputC);
			tagJ = readFile(outputJ);
			tagCJ.putAll(tagC);
			tagCJ.putAll(tagJ);
			sizes = tagCJ.size();
			test1 = readFile(outputTest1);
			test2 = readFile(outputTest2);  
			test4 = readFile(outputTest4);  
			
			double resultC_test1,resultJ_test1, resultC_test2,resultJ_test2,
					resultC_test4,resultJ_test4 ;
			//0.75，0.25是分别是C,J类文档概率
			resultC_test1 = Result(0.75,tagC,test1);
			resultJ_test1 = Result(0.25,tagJ,test1);
			resultC_test2 = Result(0.75,tagC,test2);
			resultJ_test2 = Result(0.25,tagJ,test2); 
			resultC_test4 = Result(0.75,tagC,test4);
			resultJ_test4 = Result(0.25,tagJ,test4); 
			System.out.println("--------------------------------Resut------------------------------");
//			System.out.println("-----resultC_test1:" + resultC_test1+",----normalized:"+resultC_test1/(resultC_test1+resultJ_test1));
//			System.out.println("-----resultJ_test1:" + resultJ_test1+",----normalized:"+resultJ_test1/(resultC_test1+resultJ_test1));
//			System.out.println("-----resultC_test2:" + resultC_test2+",----normalized:"+resultC_test2/(resultC_test2+resultJ_test2));
//			System.out.println("-----resultJ_test2:" + resultJ_test2+",----normalized:"+resultJ_test2/(resultC_test2+resultJ_test2));
			
			System.out.println("-----resultC_test4:" + resultC_test4+",----normalized:"+resultC_test4/(resultC_test4+resultJ_test4));
			System.out.println("-----resultJ_test4:" + resultJ_test4+",----normalized:"+resultJ_test4/(resultC_test4+resultJ_test4));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static Map<String, Double> readFile(Path path){
		Configuration conf = configFun();//得到远程配置信息
		FileSystem fs; 
		Map<String, Double> tmpMap = new HashMap<String, Double>();
			try {
				//文件读入读出流
				fs = FileSystem.get(conf);
				FSDataInputStream in = fs.open(path);  
				InputStreamReader isr = new InputStreamReader(in); 
				BufferedReader br = new BufferedReader(isr);  
				String line;
				while (((line = br.readLine()) != null)) {
					String[] tmp = line.split(":"); // key为单词，value为单词出现频数
					tmpMap.put(tmp[0], Double.valueOf(tmp[1].trim()));
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return tmpMap; 
	}

	// 给出类标，和单词对应概率集，返回该类下的概率->即某个测试集是某X类文档类型的概率
	//probalilityTagX为X类文档占总文档的概率，tagX是某个X文档类的单词以及单词概率，testX待测测试集
	public static double Result(double probalilityTagX,Map<String, Double> tagX, Map<String, Double> testX) {
		Map<String, Double> calculPro = new HashMap<String, Double>();
		calculPro = calculProbaby(tagX);
		double probalityTag = 0.0; 
		probalityTag = ResultProbability(calculPro, testX)*probalilityTagX;
		return probalityTag;
	}

	// 计算每个单词概率
	public static Map<String, Double> calculProbaby(Map<String, Double> tag) { 
		//用于存储返回的<单词：频率>
		Map<String, Double> tmp = new HashMap<String, Double>();
		
		//由于计算复杂度n远远小于n*n，又因为避免污染tagCJ，所以引入copyTagCJ量操作，
		//如果每次for循环遍历tagCJ改变和tag类中不同值回有n*n的计算复杂度,这样效率太低
		//为了确保每个类中每个单词都有出现概率，所以要在传入tag类中，把tag类赋值给copyTagCJ，重新赋值前要使copyTagCJ所有单词频数置0
		Map<String, Double> copyTagCJ = new HashMap<String, Double>();
		for (java.util.Map.Entry<String, Double> entry : tagCJ.entrySet()) {
			copyTagCJ.put(entry.getKey(), 0.0);
		} 
		double sum = 0;// total words number;
		//对频数置0后的tagCJ，对其中tag的频数更新为真实传入的tag类值
		for (java.util.Map.Entry<String, Double> entry : tag.entrySet()) {
			sum += entry.getValue(); 
			copyTagCJ.put(entry.getKey(), entry.getValue());
		}
		double value = 0.0;
		//计算概率：由于tagCJ包含所有文档单词类，所以对每个单词进行概率计算：(单词频数+1)/(文档单词数+平滑值)
		for (java.util.Map.Entry<String, Double> entry : copyTagCJ.entrySet()) {
			value = (entry.getValue() + 1) / (sum + sizes);
			tmp.put(entry.getKey(), value); 
		}
		//System.out.println("sum:" + sum + "sizes:" + sizes + ",tmp---:" + tmp);
		return tmp;
	}

	// 返回要估计的文档概率，还没乘文档类概率
	public static double ResultProbability(Map<String, Double> calculProbaby,
			Map<String, Double> test) {
		double estimation = 1.0;
		//文档概率=文档类概率*Math.pow(单词概率,单词出现频数)
		for (java.util.Map.Entry<String, Double> entry : test.entrySet()) {
			double probaby = calculProbaby.get(entry.getKey());
			double multi = entry.getValue(); 
			estimation *= Math.pow(probaby, multi);// calculProbaby.get(entry.getKey())概率
		}
		return estimation;
	}

	public static Configuration configFun() {
		Configuration conf = new YarnConfiguration();
		conf.set("fs.defaultFS", "hdfs://192.168.70.14:9000");
		conf.set("yarn.resourcemanager.address", "192.168.70.14:8032");
		conf.set("yarn.resourcemanager.scheduler.address", "192.168.70.14:8030");
		conf.set("mapreduce.framework.name", "yarn");
		conf.set("mapred.jar", "D://软件安装//Users//win7//BayesDocument.jar");
		conf.set("mapreduce.app-submission.cross-platform", "true");
		return conf;
	}

	public static void main(String[] args) throws Exception {
		
		getData();
	}
}
